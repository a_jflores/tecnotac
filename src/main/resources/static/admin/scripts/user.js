$(document).ready(function() {
	$('#table-users').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
		},
		responsive: true,
       	ajax: "/tecnotac/data/users",
        serverSide: true,
        "processing": true,
        columns: [
			{ "data": "id" },
			{ "data": "email" },
            { "data": "enabled" },
            { "data": "firstName" },
            { "data": "lastName" },
            {
				"data": "picture",
	            "render": function (data) {
	                return  '<img class="rounded-circle" height="50px" width="50px" src="'+data+'">';
	            },
	            "orderable": false
	        },
            {
				"data": "id",
	            "render": function (data) {
	                return  '<a href="/tecnotac/admin/user/view/'+data+'"><i class="bx bx-show-alt"></i></a>';
	            },
	            "orderable": false
	        }
        ],
	});

});
