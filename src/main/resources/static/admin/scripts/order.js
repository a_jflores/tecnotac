$(document).ready(function() {
	
	$.fn.editableform.buttons =
	'<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
	    '<i class="fa fa-fw fa-check"></i>' +
	    '</button>' +
	'<button type="button" class="btn btn-warning btn-sm editable-cancel">' +
	    '<i class="fa fa-fw fa-times"></i>' +
	    '</button>';
	    
    $.extend($.fn.dataTable.defaults, {
        responsive: true
    });
    
	$('#table-orders').editable({
		container:'body',
		selector:'td.status',
		url:'/tecnotac/api/order/update/orderStatus',
	    source: [
          {value: "COMPLETED", text: 'PAGADO'},
          {value: "PENDING", text: 'PENDING'},
          {value: "CANCELED", text: 'CANCELADO'},
          {value: "DELIVERED", text: 'ENTREGADO'}
       	],
		validate:function(value){
			if($.trim(value) == '')
			{
				return 'Campo requerido';
			}
		}
	});
	
	$('#table-orders').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
		},
		responsive: true,
		//"pageLength": 1,
       	ajax: "/tecnotac/data/orders",
        serverSide: true,
        "processing": true,
        columns: [
			{ "data": "id" },
			{ "data": "number" },
            { 
				"data": "created_at",
				"render": function(data) { return moment(data).format('DD/MM/YYYY', 'DD/MM/YYYY', 'es'); },
            },
            { "data": "status" },
            { "data": "paymentMethod" },
            { "data": "total" },
            {
				"data": "id",
	            "render": function (data) {
	                return  '<a href="/tecnotac/admin/order/view/'+data+'"><i class="bx bx-show-alt"></i></a>';
	            },
	            "orderable": false
	        }
        ],
        createdRow:function(row, data, rowIndex)
		{
			$.each($('td', row), function(colIndex){
				if(colIndex == 3)
				{
					$(this).attr('data-value', data.status);
					$(this).attr('class', 'status');
					$(this).attr('data-type', 'select');
					$(this).attr('data-pk', data.id);
					$(this).attr('data-title', 'Selecciona estado');
				}
			});
		}
    });

});
