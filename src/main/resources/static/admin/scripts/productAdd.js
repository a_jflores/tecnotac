$(document).ready(function() {
	
	var id = $("#id").val();	  
    if (id != "") {
		var CategoryId = $("#CategoryId").val();

		$.ajax({
	        type: 'GET',
	        contentType: "application/json; charset=utf-8",
	        url: "/tecnotac/api/categories",
	        cache: false,
	        dataType: 'json',
	        success: function(data){
	            for(var i=0; i<data.length; i++){
					if(data[i].id == CategoryId){
						$("#category").val(data[i].parent.parent.id);
						$("#category").change();
					}
	            }
	        },
	        error:function(){
	            console.log("error");
	        }
	    });
	    
		$("#category").change(function(){
			$("#subcategory").empty();	
			$("#subcategory2").empty();	
		    var categoryId = $(this).val();
		    
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/admin/sub/"+categoryId,
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            var slctSubcat=$('#subcategory'), option="";
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].id + "'>"+data[i].name + "</option>";
		            }
		            slctSubcat.append(option);
		        },
		        error:function(){
		            console.log("error");
		        }
		    });
		    
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/api/categories",
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            for(var i=0; i<data.length; i++){
						if(data[i].id == CategoryId){
							$("#subcategory").val(data[i].parent.id);
							$("#subcategory").change();
						}
		            }
		        },
		        error:function(){
		            console.log("error");
		        }
		    });
		});
		
		$("#subcategory").change(function(){
			$("#subcategory2").empty();	
		    var categoryId = $(this).val();
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/admin/sub/"+categoryId,
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            var slctSubcat=$('#subcategory2'), option="";
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].id + "'>"+data[i].name + "</option>";
		            }
		            slctSubcat.append(option);
		        },
		        error:function(){
		            console.log("error");
		        }
		    });
		    
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/api/categories",
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            for(var i=0; i<data.length; i++){
						if(data[i].id == CategoryId){
							$("#subcategory2").val(data[i].id);
							
						}
		            }
		        },
		        error:function(){
		            console.log("error");
		        }
		    });
		 
		});
	    
	} else {
		
		$("#category").change(function(){
			$("#subcategory").empty();	
			$("#subcategory2").empty();	
			$("#subcategory").append("<option value='' selected>Seleccione una subcategroría 1</option>");
			$("#subcategory2").append("<option value='' selected>Seleccione una subcategroría 2</option>");
		    var categoryId = $(this).val();
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/admin/sub/"+categoryId,
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            var slctSubcat=$('#subcategory'), option="";
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].id + "'>"+data[i].name + "</option>";
		            }
		            slctSubcat.append(option);
		        },
		        error:function(){
		            console.log("error");
		        }
		    });
		});
	
		$("#subcategory").change(function(){
			$("#subcategory2").empty();	
			$("#subcategory2").append("<option value='' selected>Seleccione una subcategroría 2</option>");
		    var categoryId = $(this).val();
		    $.ajax({
		        type: 'GET',
		        contentType: "application/json; charset=utf-8",
		        url: "/tecnotac/admin/sub/"+categoryId,
		        cache: false,
		        dataType: 'json',
		        success: function(data){
		            var slctSubcat=$('#subcategory2'), option="";
		            for(var i=0; i<data.length; i++){
		                option = option + "<option value='"+data[i].id + "'>"+data[i].name + "</option>";
		            }
		            slctSubcat.append(option);
		        },
		        error:function(){
		            console.log("error");
		        }
		
		    });
		});
	}
	
	var idProduct = $("#id").val();
	if(idProduct!="") {
		$.ajax({
	        type: 'GET',
	        dataType: 'json',
	        url: "/tecnotac/api/getproduct/"+idProduct,
	        cache: false,
	        dataType: 'json',
	        success: function(data){
				$("#input-25").fileinput({
					language:"es",
				    theme: "fas",
				    showUpload: false,
				    showRemove: false,
				    maxFileCount: 5,
				    allowedFileExtensions: ["jpg", "png","jpeg"],
				    initialPreview: [
				        "/tecnotac/productimgs/"+data.image[0].name,
				        "/tecnotac/productimgs/"+data.image[1].name,
				        "/tecnotac/productimgs/"+data.image[2].name,
				        "/tecnotac/productimgs/"+data.image[3].name,
				        "/tecnotac/productimgs/"+data.image[4].name
				    ],
				    initialPreviewAsData: true,
					initialPreviewFileType: 'image'
				});
	        },
	        error:function(){
	            console.log("error");
	        }
	    });
	} else {
		console.log(idProduct);
		$("#input-25").fileinput({
		    theme: "fas",
		    showUpload: false,
		    allowedFileExtensions: ["jpg", "png","jpeg"]
		});
	}
	
});
