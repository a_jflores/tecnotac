$(document).ready(function() {
	$('#quick_view').on('hidden.bs.modal', function () {
        $(this).find('quick_view').trigger('reset');
    });
    
    $('.modal_view').on('click', function () {
		$('#quick_view').trigger("reset");
		$(".pro-large-img1").empty();
		$(".pro-large-img2").empty();
		$(".pro-large-img3").empty();
		$(".pro-large-img4").empty();
		$(".pro-large-img5").empty();
		$(".pro-nav-thumb1").empty();
		$(".pro-nav-thumb2").empty();
		$(".pro-nav-thumb3").empty();
		$(".pro-nav-thumb4").empty();
		$(".pro-nav-thumb5").empty();
		var product_id = $(this).data('product-id');
        console.log(product_id);
		$.ajax({
            url: '/tecnotac/api/product/'+product_id,
            Type: 'GET',
            dataType: 'json',
            success: function (data) {
				$("#product_name").prop("href", "/tecnotac/shop/viewproduct/"+data.id)
                $("#product_name").text(data.name);
                $("#product_stock").text(data.stock+" en stock");
                $("#product_price").text("S/ "+data.price);
                $("#product_description").text(data.description);
                $.each(data.image, function (key, item) {
					$('<img src="/tecnotac/productimgs/'+item.name+'" alt="" />').appendTo($(".pro-large-img"+(key+1)));
					$('<img src="/tecnotac/productimgs/'+item.name+'" alt="" />').appendTo($(".pro-nav-thumb"+(key+1)));
	            });
            },
            error: function () {
                console.log("Error");
            }
        });
    });
});
