$(document).ready(function() {
	
	var firstName = $("#firstName").val();
	var lastName = $("#lastName").val();
	var email = $("#email").val();
	var address = $("#address").val();
	var addressOptional = $("#addressOptional").val();
	var codePostal = $("#codePostal").val();
	var departament = $("#departament").val();
	var province = $("#province").val();
	var district = $("#district").val();
	var country = $("#country_order").val();
	var phone = $("#phone").val();
	var note = $("#note").val();
	var total = $("#total").val();
	var paymentmethod = $("#paymentmethod").val();
	
	onCheckout();
	function onCheckout()
	{
		var order = {
		  "amount": total,
		  "currency": "PEN",
		  "customer": {
		    "email": email,
		    "billingDetails": {
		      "firstName": firstName,
		      "lastName": lastName,
		      "address": address,
		      "address2": addressOptional,
		      "district": district,
		      "zipCode": codePostal,
		      "city": departament,
		      "country": country,
		      "cellPhoneNumber": phone
		    }
		  }
		}
	    getFormToken(order);
	}
	
	function getFormToken(order){
		$.ajax({
	        url: '/tecnotac/api/payment/init',
	        contentType: "application/json",
	        Type: 'POST',
	        data: order,
	        dataType: 'json',
	        cache: false,
	        timeout: 600000,
		    beforeSend: function() {
				$(".preloader").show();
	            document.getElementById("loading-msg").innerHTML = "ESPERE UNOS SEGUNDOS ...";
	        },
	        success: function (data) {
				displayPaymentForm(data.answer.formToken);
	            //console.log(data);
	        },
	        error: function () {
	            console.log("error post");
	        },
		    complete: function() {
	            $(".preloader").hide();
	        }
	    });
	}
	
	function displayPaymentForm(formToken)
	{
	    document.getElementById('paymentForm').style.display = 'block';
	    KR.setFormToken(formToken);
	    KR.onSubmit(onPaid);
	}
	
	function onPaid(event) {
	  if (event.clientAnswer.orderStatus === "PAID") {
		
		var ipAddress = event.clientAnswer.customer.extraDetails.ipAddress;
		var uuid = event.clientAnswer.transactions[0].uuid;
		var shopId = event.clientAnswer.shopId;
		var orderStatus = event.clientAnswer.orderStatus;
		
		var payment = {
			"firstName": firstName,
	      	"lastName": lastName,
	      	"email": email,
	      	"address": address,
	      	"address2": addressOptional,
	      	"zipCode": codePostal,
	      	"city": departament,
	      	"province": province,
	      	"district": district,
	      	"country": country,
	      	"cellPhoneNumber": phone,
	      	"note": note,
	      	"total": total,
	      	"paymentmethod":paymentmethod,
	      	"ipAddress":ipAddress,
	      	"uuid": uuid,
	      	"shopId": shopId,
	      	"orderStatus": orderStatus
		};
		
	    KR.removeForms();
		
	    $.ajax({
	        type: "POST",
	        contentType: "application/json",
	        url: "/tecnotac/api/payment/success",
	        data: JSON.stringify(payment),
	        dataType: 'json',
	        cache: false,
	        timeout: 600000,
	        success: function (data) {
	            //console.log("SUCCESS : ", data);
	            window.location.href = "/tecnotac/thanks";
	        },
	        error: function (e) {
				console.log("ERROR : ", e);
	        }
	    });
	    document.getElementById("paymentSuccessful").style.display = "block";
	  } else {
	  	alert("Payment failed !");
	  }
	}

});
