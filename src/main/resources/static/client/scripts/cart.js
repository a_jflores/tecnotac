$(document).ready(function() {
	$('tr .pro-qty').click(function() {
		var id = $(this).parent().find("#id").val();
		var quantity = $(this).parent().find(".quantity").val();
		var url = "/tecnotac/updateCart";

		$.ajax({
			type: "post",
			url: url,
			data: {
				id,
				quantity
			},
			success: function() {
				location.reload();
			}
		});

	});

});
