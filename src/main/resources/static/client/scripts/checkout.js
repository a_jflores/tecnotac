$(document).ready(function() {
    $.fn.select2.defaults.set('language', 'es');
    
    $("#btn-checkout").on("click", function(){
		$('#form-checkout').submit();
	});
	
    $('#country').select2({
	    minimumInputLength: 2,
        placeholder: "Selecciona un País"
	});
	
	$('#departamento').select2({
        placeholder: "Selecciona un departamento"
	});
	
	$('#provincia').select2({
        placeholder: "Selecciona una provincia"
	});
	
	$('#distrito').select2({
        placeholder: "Selecciona una distrito"
	});
	
	$('#country').on('change', function() {
		
		//var countrySelected = $("#country option:selected").val();
		//console.log($("#country option:selected").val());
		
		/*$.ajax({
            url: 'https://restcountries.eu/rest/v2/all',
            Type: 'GET',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, item) {
                    $("#country").append('<option value=' + item.alpha2Code + '>' + item.name + '</option>');
                });
            },
            error: function () {
                console.log("No se ha podido obtener la información");
            }
        });*/
        
        return false;
	});
	
   	/*$.ajax({
        url: 'https://restcountries.eu/rest/v2/all',
        Type: 'GET',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key, item) {
                $("#country").append('<option value=' + item.alpha2Code + '>' + item.name + '</option>');
            });
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });*/
    
    $.getJSON("json/ubigeo/ubigeo_peru_2016_departamentos.js", function( data ) {
		$.each(data, function (key, item) {
        	$("#departamento").append('<option value=' + item.id + '>' + item.name + '</option>');
        });
	});
	
	$('#departamento').on('change', function() {
		$("#provincia").empty();	
		$("#distrito").empty();	
		$("#provincia").append("<option value='' disabled selected>Seleccione una provincia</option>");
		$("#distrito").append("<option value='' disabled selected>Seleccione un distrito</option>");
		
		var departamento_id = $("#departamento option:selected").val();
		var departamento_name = $("#departamento option:selected").text();
		$("#departament").val(departamento_name);
		
		$.getJSON("json/ubigeo/ubigeo_peru_2016_provincias.js",function( data ) {
			$.each(data, function (key, item) {
				if(item.department_id == departamento_id) {
					$("#provincia").append('<option value=' + item.id + '>' + item.name + '</option>');
				}
            });					
		});
	});
	
	$('#provincia').on('change', function() {	
		$("#distrito").empty();	
		$("#distrito").append("<option value='' disabled selected>Seleccione un distrito</option>");
		
		var provincia_id = $("#provincia option:selected").val();
		var provincia_name = $("#provincia option:selected").text();
		$("#province").val(provincia_name);
		
		$.getJSON("json/ubigeo/ubigeo_peru_2016_distritos.js",function( data ) {
			$.each(data, function (key, item) {
				if(item.province_id == provincia_id) {
					$("#distrito").append('<option value=' + item.id + '>' + item.name + '</option>');
				}
            });					
		});
	});
	
	$('#distrito').on('change', function() {
		var distrito_name = $("#distrito option:selected").text();
		$("#district").val(distrito_name);
	});	

});
