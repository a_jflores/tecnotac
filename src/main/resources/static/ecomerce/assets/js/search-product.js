$(document).ready(function() {
	var products = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.whitespace,
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: "/tecnotac/api/products"
	});
	
	$('#search').typeahead({
	  hint: true,
	  highlight: true,
	  minLength: 1
	},
	{
	  name: 'products',
	  source: products
	}).on('typeahead:selected', function() {
	    $("#form_search").submit();
	});
});
