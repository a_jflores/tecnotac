package com.tecnotac.ecomerce.apirestcontroller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tecnotac.ecomerce.model.Order;
import com.tecnotac.ecomerce.model.User;
import com.tecnotac.ecomerce.repository.DaoOrderRepository;
import com.tecnotac.ecomerce.repository.DaoUserRepository;

@RestController
@RequestMapping("/data")
public class AdminRestController {

	@Autowired
	private DaoOrderRepository daoOrderRepository;
	
	@Autowired
	private DaoUserRepository daoUserRepository;

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public DataTablesOutput<Order> getOrders(@Valid DataTablesInput input) {
		return daoOrderRepository.findAll(input);
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public DataTablesOutput<User> getUsers(@Valid DataTablesInput input) {
		return daoUserRepository.findAll(input);
	}
}
