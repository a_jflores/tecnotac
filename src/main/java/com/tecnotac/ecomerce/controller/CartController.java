package com.tecnotac.ecomerce.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ibm.icu.text.SimpleDateFormat;
import com.tecnotac.ecomerce.dto.ItemDTO;
import com.tecnotac.ecomerce.global.GlobalData;
import com.tecnotac.ecomerce.model.BillPayer;
import com.tecnotac.ecomerce.model.Order;
import com.tecnotac.ecomerce.model.OrderItems;
import com.tecnotac.ecomerce.myenum.PaymentMethod;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.model.ShippingAddress;
import com.tecnotac.ecomerce.model.Transaction;
import com.tecnotac.ecomerce.myenum.Status;
import com.tecnotac.ecomerce.service.BillPayerService;
import com.tecnotac.ecomerce.service.CustomUserDetailService;
import com.tecnotac.ecomerce.service.OrderItemsService;
import com.tecnotac.ecomerce.service.OrderService;
import com.tecnotac.ecomerce.service.ProductService;
import com.tecnotac.ecomerce.service.ShippingAddressService;
import com.tecnotac.ecomerce.service.TransactionService;

@Controller
public class CartController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderItemsService orderItemsService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
	private ShippingAddressService shippingAddressService;
	
	@Autowired
	private BillPayerService billPayerService;
	
	@Autowired
	private CustomUserDetailService customUserDetailService;
	
	@PostMapping("/addToCart")
	public String addToCart(@RequestParam("id") int id, @RequestParam("quantity") int quantity) {
		Product product = productService.getProductById(id).get();
		boolean located = false;
		int position = 0;
		BigDecimal newsubtotal = new BigDecimal (0);
		BigDecimal newquantity = new BigDecimal (1);
		
		ItemDTO itemDTO = new ItemDTO();
		itemDTO.setId(product.getId());
		itemDTO.setName(product.getName());
		itemDTO.setPrice(product.getPrice());
		itemDTO.setSubtotal(product.getPrice().multiply(new BigDecimal(quantity)));
		itemDTO.setQuantity(quantity);
		itemDTO.setNameImage(product.getImage().get(0).getName());
		itemDTO.setParentCategory(product.getCategory().getParent().getId());
		
		for (int x = 0; x < GlobalData.cartItem.size(); x++) {
			ItemDTO p = GlobalData.cartItem.get(x);
			if (p.getId().equals(itemDTO.getId())) {
				located = true;
				position = x;
				break;
			}
		}
		
		if (located) {
			newquantity = new BigDecimal(GlobalData.cartItem.get(position).getQuantity()).add(new BigDecimal(quantity));
			GlobalData.cartItem.get(position).setQuantity(newquantity.intValue());
			newsubtotal = GlobalData.cartItem.get(position).getPrice().multiply(newquantity);
			GlobalData.cartItem.get(position).setSubtotal(newsubtotal);
		} else {
			GlobalData.cartItem.add(itemDTO);
		}
		return "redirect:/shop";
	}
	
	@PostMapping("/updateCart")
	public String updateCart(@RequestParam("id") int id, @RequestParam("quantity") int quantity) {

		Product product = productService.getProductById(id).get();
		boolean located = false;
		BigDecimal newsubtotal = new BigDecimal(0);
		int position = 0;
		
		ItemDTO itemDTO = new ItemDTO();
		itemDTO.setId(product.getId());
		itemDTO.setName(product.getName());
		itemDTO.setPrice(product.getPrice());
		itemDTO.setSubtotal(product.getPrice().multiply(new BigDecimal(quantity)));
		itemDTO.setQuantity(quantity);
		itemDTO.setNameImage(product.getImage().get(0).getName());
		itemDTO.setParentCategory(product.getCategory().getParent().getId());

		for (int x = 0; x < GlobalData.cartItem.size(); x++) {
			ItemDTO p = GlobalData.cartItem.get(x);
			if (p.getId().equals(itemDTO.getId())) {
				located = true;
				position = x;
				break;
			}
		}

		if (located) {
			GlobalData.cartItem.get(position).setQuantity(quantity);
			newsubtotal = GlobalData.cartItem.get(position).getPrice().multiply(new BigDecimal(quantity));
			GlobalData.cartItem.get(position).setSubtotal(newsubtotal);
		} 
		System.out.println("carrito: " + GlobalData.cartItem);
		return "redirect:/cart";
	}

	@GetMapping("/cart")
	public String cartGet(Model model) {
		model.addAttribute("cartCount", GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart", GlobalData.cartItem);
		return "/views/cart/cart";
	}

	@GetMapping("/cart/removeItem/{index}")
	public String cartItemRemove(@PathVariable int index) {
		GlobalData.cartItem.remove(index);
		return "redirect:/cart";
	}

	@GetMapping("/checkout")
	public String checkout(Model model, Authentication authentication) {
		BigDecimal total = new BigDecimal(0);
		total = GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2);
		if (total.compareTo(BigDecimal.ZERO) == 0) {
			return "redirect:/shop";
		} else {
			model.addAttribute("cartCount", GlobalData.cartItem.size());
			model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
			model.addAttribute("cart", GlobalData.cartItem);
			model.addAttribute("userId",GlobalData.user.get(0).getId());
			model.addAttribute("userEmail",GlobalData.user.get(0).getEmail());
			model.addAttribute("userFirstName",GlobalData.user.get(0).getFirstName());
			model.addAttribute("userLastName",GlobalData.user.get(0).getLastName());
			return "/views/checkout/checkout";
		}
	}
	
	@PostMapping("/checkout")
	public String checkoutPost( Model model,
			HttpServletRequest request,
			RedirectAttributes redirectAttributes,
			@RequestParam("id") int id,
			@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam("email") String email,
			@RequestParam("departament") String departament,
			@RequestParam("departament") String province,
			@RequestParam("district") String district,
			@RequestParam("address") String address,
			@RequestParam("addressOptional") String addressOptional,
			@RequestParam("codePostal") String codePostal,
			@RequestParam("phone") String phone,
			@RequestParam("note") String note,
			@RequestParam("paymentmethod") String paymentmethod) {
		
		if (paymentmethod.equals("IZIPAY")) {
			model.addAttribute("cartCount", GlobalData.cartItem.size());
			model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
			model.addAttribute("cart", GlobalData.cartItem);
			model.addAttribute("userId",GlobalData.user.get(0).getId());
			model.addAttribute("userEmail",GlobalData.user.get(0).getEmail());
			model.addAttribute("userFirstName",GlobalData.user.get(0).getFirstName());
			model.addAttribute("userLastName",GlobalData.user.get(0).getLastName());
			model.addAttribute("id", id);
			model.addAttribute("firstName", firstName);
			model.addAttribute("lastName", lastName);
			model.addAttribute("email", email);
			model.addAttribute("departament", departament);
			model.addAttribute("province", province);
			model.addAttribute("district", district);
			model.addAttribute("address", address);
			model.addAttribute("addressOptional", addressOptional);
			model.addAttribute("codePostal", codePostal);
			model.addAttribute("phone", phone);
			model.addAttribute("note", note);
			model.addAttribute("paymentmethod", paymentmethod);
			return "/views/checkout/izipay";
		}
		
		Random random = new Random();
		List<ItemDTO> cart = GlobalData.cartItem;
		BigDecimal total = new BigDecimal(0);
		total = GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2);
		
		Date date = new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String ipAddress = request.getHeader("x-forwarded-for");
		String orderStatus = "UNPAID";
		String uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "");
		String shopId = "tecnotac";
		
		Transaction transaction = new Transaction();
		transaction.setIpAddress(ipAddress);
		transaction.setOrderStatus(orderStatus);
		transaction.setShopId(shopId);
		transaction.setTotal(total);
		transaction.setUuid(uuid);
		transactionService.addTransaction(transaction);
		
		ShippingAddress shippingAddress = new ShippingAddress();
		shippingAddress.setAddress(address);
		shippingAddress.setAddressOpcional(addressOptional);
		shippingAddress.setPostalCode(codePostal);
		shippingAddress.setDepartament(departament);
		shippingAddress.setProvince(province);
		shippingAddress.setDistrict(district);
		shippingAddressService.addShippingAddress(shippingAddress);
		
		BillPayer billPayer = new BillPayer();
		billPayer.setShippingAddress(shippingAddressService.getShippingAddressById(shippingAddress.getId()).get());
		billPayer.setEmail(email);
		billPayer.setFirstName(firstName);
		billPayer.setLastName(lastName);
		billPayer.setPhone(phone);
		billPayer.setCreated_at(date);
		billPayerService.addBillPayer(billPayer);
		
		Order order = new Order();
		order.setPaymentMethod(PaymentMethod.valueOf(paymentmethod));
		order.setStatus(Status.PENDING);
		order.setTotal(total);
		order.setUser(customUserDetailService.getUserById(id).get());
		order.setNumber((char)(random.nextInt(26) + 'A') +  "-" + formatter.format(date).replaceAll("[/: ]", ""));
		order.setNote(note);
		order.setCreated_at(date);
		orderService.addOrder(order);
		
		for (ItemDTO item : cart) {
			OrderItems orderItems = new OrderItems();
			orderItems.setOrder(orderService.getOrderById(order.getId()).get());
			orderItems.setProduct(productService.getProductById(item.getId()).get());
			orderItems.setPrice(item.getPrice());
			orderItems.setQuantity(item.getQuantity());
			orderItems.setTotal(item.getSubtotal());
			orderItemsService.addOrderItems(orderItems);
			
			Product product = new Product();
			product.setId(item.getId());
			product.setStock(item.getStock()-item.getQuantity());
			productService.updateStockProduct(product);
		}
		
		GlobalData.cartItem.clear();
		
		return "redirect:/thanks";
	}
	
	@GetMapping("/izipay")
	public String izipay(Model model, @ModelAttribute("note") String note, HttpSession session) {
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		session.setAttribute(note, note);
		return "/views/checkout/izipay";
	}
	
	@GetMapping("/thanks")
	public String thanks(Model model) {
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/thanks/thanks";
	}
	
	@PostMapping("/thanks")
	public String postThanks(Model model) {
		GlobalData.cartItem.clear();
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/thanks/thanks";
	}

}
