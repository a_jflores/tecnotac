package com.tecnotac.ecomerce.controller;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ibm.icu.text.SimpleDateFormat;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.dto.ItemDTO;
import com.tecnotac.ecomerce.global.GlobalData;
import com.tecnotac.ecomerce.model.BillPayer;
import com.tecnotac.ecomerce.model.Order;
import com.tecnotac.ecomerce.model.OrderItems;
import com.tecnotac.ecomerce.myenum.PaymentMethod;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.model.ShippingAddress;
import com.tecnotac.ecomerce.myenum.Status;
import com.tecnotac.ecomerce.model.Transaction;
import com.tecnotac.ecomerce.repository.UserRepository;
import com.tecnotac.ecomerce.service.BillPayerService;
import com.tecnotac.ecomerce.service.CustomUserDetailService;
import com.tecnotac.ecomerce.service.OrderItemsService;
import com.tecnotac.ecomerce.service.OrderService;
import com.tecnotac.ecomerce.service.ProductService;
import com.tecnotac.ecomerce.service.ShippingAddressService;
import com.tecnotac.ecomerce.service.TransactionService;

@RestController
@RequestMapping("/api")
public class IziPayRestController {
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	OrderItemsService orderItemsService;
	
	@Autowired
	ShippingAddressService shippingAddressService;
	
	@Autowired
	BillPayerService billPayerService;
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	CustomUserDetailService customUserDetailService;

	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/payment/init")
	@ResponseBody
	public ResponseEntity<String> getIziPay(HttpServletRequest req, @RequestParam HashMap<String, Object> order) {

		String url = "https://api.micuentaweb.pe/api-payment/V4/Charge/CreatePayment";		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic ODkyODk3NTg6dGVzdHBhc3N3b3JkXzd2QXR2TjQ5RThBZDZlNmloTXFJT3ZPSEM2UVY1WUttSVhneGlzTW0wVjdFcQ==");
		
		BigDecimal total = new BigDecimal(0);
		total = GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2);
		String totalString = total.toString();
		String newTotal = totalString.replace(".", "");
		String email = order.get("customer[email]").toString();
		String firstName = order.get("customer[billingDetails][firstName]").toString();
		String lastName = order.get("customer[billingDetails][lastName]").toString();
		String address = order.get("customer[billingDetails][address]").toString();
		String address2 = order.get("customer[billingDetails][address2]").toString();
		String district = order.get("customer[billingDetails][district]").toString();
		String zipCode = order.get("customer[billingDetails][zipCode]").toString();
		String city = order.get("customer[billingDetails][city]").toString();
		String country = order.get("customer[billingDetails][country]").toString();
		String cellPhoneNumber = order.get("customer[billingDetails][cellPhoneNumber]").toString();
		
		if (address2.isEmpty()) {
			address2 = "na";
		}
		
		HashMap<String, Object> innerMap2 = new HashMap<>();
		innerMap2.put("firstName", firstName);
		innerMap2.put("laststName", lastName);
		innerMap2.put("address", address);
		innerMap2.put("address2", address2);
		innerMap2.put("district", district);
		innerMap2.put("zipCode", zipCode);
		innerMap2.put("city", city);
		innerMap2.put("country", country);
		innerMap2.put("cellPhoneNumber", cellPhoneNumber);
		
		HashMap<String, Object> innerMap1 = new HashMap<>();
		innerMap1.put("email", email);
		innerMap1.put("billingDetails", innerMap2);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("amount", newTotal);
		map.put("currency", "PEN");
		map.put("customer", innerMap1);

		HttpEntity<HashMap<String, Object>> request = new HttpEntity<HashMap<String, Object>>(map, headers);
		ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
		return response;
	}
	
	@PostMapping("/payment/success")
	@ResponseBody
	public HashMap<String, Object> postPaymentSuccess(@RequestBody HashMap<String, Object> answer) {		
		
		JSONObject json = new JSONObject(answer);
		System.out.println(json);
		
		String note = json.get("note").toString();
		String paymentmethod = json.get("paymentmethod").toString();
		
		String firstName = json.get("firstName").toString();
		String lastName = json.get("lastName").toString();
		String email = json.get("email").toString();
		String phone = json.get("cellPhoneNumber").toString();
		
		String address = json.get("address").toString();
		String address2 = json.get("address2").toString();
		String codePostal = json.get("zipCode").toString();
		String departament = json.get("city").toString();
		String province = json.get("province").toString();
		String district = json.get("district").toString();
		
		String ipAddress = json.get("ipAddress").toString();
		String orderStatus = json.get("orderStatus").toString();
		String uuid = json.get("uuid").toString();
		String shopId = json.get("shopId").toString();
		
		System.out.println("POST json: " +json);
		Random random = new Random();
		List<ItemDTO> cart = GlobalData.cartItem;
		BigDecimal total = new BigDecimal(0);
		total = GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2);
		int id = GlobalData.user.get(0).getId();
		Date date = new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss"); 
		
		Transaction transaction = new Transaction();
		transaction.setIpAddress(ipAddress);
		transaction.setOrderStatus(orderStatus);
		transaction.setShopId(shopId);
		transaction.setTotal(total);
		transaction.setUuid(uuid);
		transactionService.addTransaction(transaction);
		
		ShippingAddress shippingAddress = new ShippingAddress();
		shippingAddress.setAddress(address);
		shippingAddress.setAddressOpcional(address2);
		shippingAddress.setPostalCode(codePostal);
		shippingAddress.setDepartament(departament);
		shippingAddress.setProvince(province);
		shippingAddress.setDistrict(district);
		shippingAddressService.addShippingAddress(shippingAddress);
		
		BillPayer billPayer = new BillPayer();
		billPayer.setShippingAddress(shippingAddressService.getShippingAddressById(shippingAddress.getId()).get());
		billPayer.setEmail(email);
		billPayer.setFirstName(firstName);
		billPayer.setLastName(lastName);
		billPayer.setPhone(phone);
		billPayer.setCreated_at(date);
		billPayerService.addBillPayer(billPayer);
		
		Order order = new Order();
		order.setPaymentMethod(PaymentMethod.valueOf(paymentmethod));
		order.setStatus(Status.COMPLETED);
		order.setTotal(total);
		order.setUser(customUserDetailService.getUserById(id).get());
		order.setNumber((char)(random.nextInt(26) + 'A') +  "-" + formatter.format(date).replaceAll("[/: ]", ""));
		order.setNote(note);
		order.setCreated_at(date);
		order.setTransaction(transaction);
		order.setBillpayer(billPayer);
		orderService.addOrder(order);
		
		for (ItemDTO item : cart) {
			OrderItems orderItems = new OrderItems();
			orderItems.setOrder(order);
			orderItems.setProduct(productService.getProductById(item.getId()).get());
			orderItems.setPrice(item.getPrice());
			orderItems.setQuantity(item.getQuantity());
			orderItems.setTotal(item.getSubtotal());
			orderItemsService.addOrderItems(orderItems);
			
			Product product = new Product();
			product.setId(item.getId());
			product.setStock(item.getStock()-item.getQuantity());
			productService.updateStockProduct(product);
		} 
		
		GlobalData.cartItem.clear();
		return answer;
	}
	
}
