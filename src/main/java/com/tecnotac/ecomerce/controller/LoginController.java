package com.tecnotac.ecomerce.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tecnotac.ecomerce.dto.ItemDTO;
import com.tecnotac.ecomerce.dto.UserDTO;
import com.tecnotac.ecomerce.global.GlobalData;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.model.Role;
import com.tecnotac.ecomerce.model.User;
import com.tecnotac.ecomerce.repository.RoleRepository;
import com.tecnotac.ecomerce.repository.UserRepository;
import com.tecnotac.ecomerce.service.CustomUserDetailService;
import com.tecnotac.ecomerce.service.OrderService;

@Controller
public class LoginController {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	CustomUserDetailService customUserDetailService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	OrderService orderService;

	@GetMapping("/login")
	public String login(Model model) {
		GlobalData.user.clear();
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/account/login";
	}
	
	@GetMapping("/myaccount")
	public String myAccount(Model model, Authentication authentication) {
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		String email = authentication.getName();
		UserDTO userDTO = new UserDTO();
		
        if(email.contains("@")) {
        	User user = customUserDetailService.getUserByEmail(email).get();
        	userDTO.setId(user.getId());
        	userDTO.setFirstName(user.getFirstName());
        	userDTO.setLastName(user.getLastName());
        	userDTO.setEmail(user.getEmail());
        	model.addAttribute("userDTO",userDTO);
    		GlobalData.user.add(customUserDetailService.getUserByEmail(user.getEmail()).get());
        } else {
        	Map<String , Object> userDetails = ((DefaultOidcUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getAttributes();
        	/*String firstGoogle = userDetails.get("given_name").toString();
        	String lastNameGoogle = userDetails.get("family_name").toString();*/
        	String emailGoogle = userDetails.get("email").toString();
        	User user = customUserDetailService.getUserByEmail(emailGoogle).get();
        	userDTO.setId(user.getId());
        	userDTO.setFirstName(user.getFirstName());
        	userDTO.setLastName(user.getLastName());
        	userDTO.setEmail(user.getEmail());
        	model.addAttribute("userDTO",userDTO);
        	GlobalData.user.add(customUserDetailService.getUserByEmail(emailGoogle).get());
        }
        
        model.addAttribute("orders",orderService.getAllOrderByUser(userDTO.getId()));
		
		return "/views/account/myaccount";
	}
	
//	@PostMapping("/myaccount/update")
//	public String myaccountPost(@Valid @ModelAttribute("userDTO") UserDTO userDTO,
//			BindingResult result, Model model,
//			HttpServletRequest request) throws ServletException {
//		
//		if (!userDTO.getPassword().equals(userDTO.getConfirmPassword())) {
//			result.addError(new FieldError("userDTO", "confirmPassword", "Las contraseñas no coiciden"));
//		}
//		
//		if (result.hasErrors()) {
//			return "/views/account/myaccount";
//		}
//		
//		User user = new User();
//		user.setFirstName(userDTO.getFirstName());
//		user.setLastName(userDTO.getLastName());
//		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
//		userRepository.save(user);
//		
//		return "/views/account/myaccount";
//	}

	@GetMapping("/register")
	public String register(Model model) {
		model.addAttribute("userDTO",new UserDTO());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/account/register";
	}

	@PostMapping("/register")
	public String registerPost(@Valid @ModelAttribute("userDTO") UserDTO userDTO,
			BindingResult result, RedirectAttributes attribute,
			HttpServletRequest request) throws ServletException{
		
		Optional<User> emailFound = customUserDetailService.getUserByEmail(userDTO.getEmail());
		
		if (emailFound.isPresent()) {
			result.addError(new FieldError("userDTO", "email", "Email no disponible"));
		} 
		
		if (!userDTO.getPassword().equals(userDTO.getConfirmPassword())) {
			result.addError(new FieldError("userDTO", "confirmPassword", "Las contraseñas no coiciden"));
		}
		
		if (result.hasErrors()) {
			return "/views/account/register";
		}
		
		User user = new User();
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		user.setEnabled(true);
		List<Role> roles = new ArrayList<>();
		roles.add(roleRepository.findById(2).get());
		user.setRoles(roles);
		userRepository.save(user);
		request.login(user.getEmail(), userDTO.getPassword());
		attribute.addFlashAttribute("success", "Usuario creado!");
		
		return "redirect:/myaccount";
		
	}

}
