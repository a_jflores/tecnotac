package com.tecnotac.ecomerce.controller;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ibm.icu.text.SimpleDateFormat;
import com.tecnotac.ecomerce.dto.ProductDTO;
import com.tecnotac.ecomerce.model.Category;
import com.tecnotac.ecomerce.model.Image;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.model.User;
import com.tecnotac.ecomerce.repository.OrderItemsRepository;
import com.tecnotac.ecomerce.service.CategoryService;
import com.tecnotac.ecomerce.service.CustomUserDetailService;
import com.tecnotac.ecomerce.service.ImageService;
import com.tecnotac.ecomerce.service.OrderService;
import com.tecnotac.ecomerce.service.ProductService;

@Controller
public class AdminController{
	
	//public static String uploadDir = System.getProperty("user.dir") + "/src/main/resources/static/productImages";
	//Mode developer
	public static String uploadDir = System.getProperty("user.dir") + "\\productimgs\\";
	public static int counting = 1;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CustomUserDetailService customUserDetailService;
	
	@Autowired
	private OrderItemsRepository orderItemsRepository;
	
	@Autowired
	private ImageService imageService;
	
	@GetMapping("/admin/dashboard")
	public String adminHome(Model model) {
		model.addAttribute("totalSales", orderService.totalSales());
		model.addAttribute("totalOrders", orderService.totalOrders());
		model.addAttribute("totalUsers", orderService.totalOrders());
		model.addAttribute("totalClients", orderService.totalOrders());
		model.addAttribute("completedOrders", orderService.completedOrders());
		model.addAttribute("pendingOrders", orderService.pendingOrders());
		model.addAttribute("canceledOrders", orderService.canceledOrders());
		model.addAttribute("deliveredOrders", orderService.deliveredOrders());
		model.addAttribute("bestSellingProduct", orderItemsRepository.bestSellingProduct());
		return "/views/home/adminHome";
	}
	
	@GetMapping("/admin/categories")
	public String getCategories(Model model) {
		model.addAttribute("categories", categoryService.getAllCategoryParent());
		return "/views/category/categories";
	}
	
	@GetMapping("/admin/categories/add")
	public String getCategoriesAdd(Model model) {
		model.addAttribute("category", new Category());
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("title", "Nueva categoría");
		return "/views/category/categoriesAdd";
	}
	
	@PostMapping("/admin/categories/add")
	public String postCategoriesAdd(@ModelAttribute("category") Category category) {
		categoryService.addCategory(category);
		return "redirect:/admin/categories";
	}
	
	@GetMapping("/admin/categories/update/{id}")
	public String updateCategory(@PathVariable int id, Model model) {
		Optional<Category> category = categoryService.getCategoryById(id);
		if (category.isPresent()) {
			model.addAttribute("category", category.get());
			model.addAttribute("title", "Editar categoría");
			model.addAttribute("action", "update");
			return "/views/category/categoriesAdd";
		} else {
			return "/views/error/404";
		}
	}
	
	@GetMapping("/admin/categories/delete/{id}")
	public String deleteCategory(@PathVariable int id) {
		categoryService.removeCategoryById(id);
		return "redirect:/admin/categories";
	}
	
	@GetMapping("/admin/subcategories/{id}")
	public String getSubCategories(@PathVariable int id, Model model) {
		model.addAttribute("categories", categoryService.getAllCategoryChildren(id));
		return "/views/category/subcategories";
	}
	
	@GetMapping("/admin/subcategoriesfinal/{id}")
	public String getSubCategoriesFinal(@PathVariable int id, Model model) {
		model.addAttribute("categories", categoryService.getAllCategoryChildren(id));
		return "/views/category/subcategoriesfinal";
	}
	
	@GetMapping("/admin/subcategories/update/{id}")
	public String updateSubCategory(@PathVariable int id, Model model) {
		Optional<Category> category = categoryService.getCategoryById(id);
		if (category.isPresent()) {
			model.addAttribute("category", category.get());
			model.addAttribute("title", "Editar sub categoría");
			model.addAttribute("action", "update");
			return "/views/category/subcategoriesAdd";
		} else {
			return "/views/error/404";
		}
	}
	
	@PostMapping("/admin/subcategories/update")
	public String postSubCategoriesUpdate(@ModelAttribute("category") Category category) {
		categoryService.addCategory(category);
		return "redirect:/admin/subcategories/"+category.getParent().getId();
	}
	
	@RequestMapping(value = "/admin/sub/{categoryId}", method = RequestMethod.GET)
	public ResponseEntity<List<Category>> getAllSubcategories(@PathVariable("categoryId") int categoryId) {
		return new ResponseEntity<List<Category>>(categoryService.getAllCategoryChildren(categoryId), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/categories", method = RequestMethod.GET)
	public ResponseEntity<List<Category>> getAllCategories() {
		return new ResponseEntity<List<Category>>(categoryService.getAllCategory(), HttpStatus.OK);
	}
	
	//Product Section
	@GetMapping("/admin/products")
	public String getProducts(Model model) {
		model.addAttribute("products", productService.getAllProduct());
		return "/views/product/products";
	}
	
	@GetMapping("/admin/products/add")
	public String getProductsAdd(Model model) {
		model.addAttribute("productDTO", new ProductDTO());
		model.addAttribute("categories", categoryService.getAllCategoryParent());
		model.addAttribute("title", "Nuevo producto");
		return "/views/product/productsAdd";
	}
	
//    List<String> fileNames = new ArrayList<>();
//    
//    Arrays.asList(files).stream().forEach(file -> {
//    	
//    	Image imageurl = new Image();
//    	imageurl.setUrl(file.getOriginalFilename());
//    	imageService.addImage(imageurl);
//        fileNames.add(file.getOriginalFilename());
//		Path fileNamePath = Paths.get(uploadDir, file.getOriginalFilename());
//		try {
//			Files.write(fileNamePath, file.getBytes());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    });
//    
//    System.out.println(fileNames);
	
	@PostMapping("/admin/products/add")
	public String postProductsAdd(@ModelAttribute("productDTO") ProductDTO productDTO, 
			@RequestParam("subcategory") int subcategory,
			@RequestParam("productImage[]") MultipartFile[] files,
			@RequestParam("imgName") String imgName, HttpServletRequest request) throws IOException{
		
		//Mode production
		//String uploadDir = request.getServletContext().getRealPath("/productimgs/");		
		
		Date date = new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss"); 
		String imageNameDate = formatter.format(date).replaceAll("[/: ]", "");
		
		Product product = new Product();
		product.setId(productDTO.getId());
		product.setName(productDTO.getName());
		product.setCategory(categoryService.getCategoryById(subcategory).get());
		product.setPrice(productDTO.getPrice());
		product.setWeight(productDTO.getWeight());
		product.setStock(productDTO.getStock());
		product.setDescription(productDTO.getDescription());
		product.setCreated_at(date);
		
//		String imageUUID = "";
//		String getSuffix = "";
//		if (!files.isEmpty()) {
//			imageUUID = files.getOriginalFilename();
//			getSuffix = imageUUID.substring(imageUUID.lastIndexOf("."));
//			Path fileNamePath = Paths.get(uploadDir, imageNameDate+getSuffix);
//			Files.write(fileNamePath, files.getBytes());
//			imageUUID = imageNameDate+getSuffix;
//		} else {
//			imageUUID = imgName;
//		}
		//product.setImageName(imageUUID);
		
		productService.addProduct(product);
		counting = 1;
	    Arrays.asList(files).stream().forEach(file -> {
			String imageUUID = "";
			String getSuffix = "";
			imageUUID = file.getOriginalFilename();
			getSuffix = imageUUID.substring(imageUUID.lastIndexOf("."));
			imageUUID = imageNameDate+"_"+counting+getSuffix;
			
	    	Image imageNameProduct = new Image();
	    	imageNameProduct.setName(imageUUID);
	    	imageNameProduct.setCreated_at(date);
	    	imageNameProduct.setProduct(product);
	    	imageService.addImage(imageNameProduct);
	        
	        Path fileNamePath = Paths.get(uploadDir, imageUUID);
			try {
				Files.write(fileNamePath, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			counting = counting +1;
	    });
	      
		return "redirect:/admin/products";
	}
	
	@PostMapping("/admin/products/update")
	public String postProductsUpdate(@ModelAttribute("productDTO") ProductDTO productDTO, 
			@RequestParam("subcategory") int subcategory,
			HttpServletRequest request) throws IOException{
		
		Date date = new Date();  

		Product product = new Product();
		product.setId(productDTO.getId());
		product.setName(productDTO.getName());
		product.setCategory(categoryService.getCategoryById(subcategory).get());
		product.setPrice(productDTO.getPrice());
		product.setWeight(productDTO.getWeight());
		product.setStock(productDTO.getStock());
		product.setDescription(productDTO.getDescription());
		product.setCreated_at(date);
		productService.addProduct(product);
		
		return "redirect:/admin/products";
	}
	
	//Upload images
//	@PostMapping("/admin/upload-image/{id}")
//	public void uploadImage(@PathVariable int id, @RequestParam("productImage[]") MultipartFile file) {
//		String url = file.getOriginalFilename();
//		String newurl = "http://localhost:8080/tecnotac/productimgs/01102021010106_1.jpg";
//		System.out.println(url);
//	}
	
	@GetMapping("/admin/product/delete/{id}")
	public String deleteProduct(@PathVariable long id) {
		productService.removeProductById(id);
		return "redirect:/admin/products";
	}
	
	@GetMapping("/admin/product/update/{id}")
	public String updateProduct(@PathVariable long id, Model model) {
		Product product = productService.getProductById(id).get();
		ProductDTO productDTO = new ProductDTO();
		productDTO.setId(product.getId());
		productDTO.setName(product.getName());
		productDTO.setCategoryId(product.getCategory().getId());
		productDTO.setPrice(product.getPrice());
		productDTO.setWeight(product.getWeight());
		productDTO.setStock(product.getStock());
		productDTO.setDescription(product.getDescription());
		
		model.addAttribute("categories", categoryService.getAllCategoryParent());
		model.addAttribute("productDTO", productDTO);
		model.addAttribute("title", "Editar producto");
		
		return "/views/product/productsUpdate";
	}
	
	@RequestMapping(value = "/api/products", method = RequestMethod.GET, produces = { "application/json"})
	public ResponseEntity<List<String>> searchProducts() {
		return new ResponseEntity<List<String>>(productService.searchProducts(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/getproducts", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProducts() {
		return new ResponseEntity<List<Product>>(productService.getAllProduct(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<Product> getProductById(@PathVariable("id") int id) {
		return new ResponseEntity<Product>(productService.getProductById(id).get(), HttpStatus.OK);
	}
	
	//Order Section
	@GetMapping("/admin/orders")
	public String getOrders() {
		return "/views/order/orders";
	}
	
	@RequestMapping(value = "/api/order/update/orderStatus", method = RequestMethod.POST)
	public @ResponseBody String updateOrder(@RequestParam(value = "value") String status, 
			@RequestParam(value = "pk") int id) {
		orderService.updateOrder(status,id);
	    return status;
	}
	
	//User Section
	@GetMapping("/admin/clients")
	public String getClients(@RequestParam Map<String, Object> params, Model model) {
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		
		PageRequest pageRequest = PageRequest.of(page, 10);
		
		Page<List<User>> pageUser = customUserDetailService.getClients(pageRequest);
		
		int totalPage = pageUser.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageUser.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		return "/views/user/users";
	}

}
