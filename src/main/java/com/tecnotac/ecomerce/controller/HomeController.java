package com.tecnotac.ecomerce.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.dto.ItemDTO;
import com.tecnotac.ecomerce.global.GlobalData;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.service.CategoryService;
import com.tecnotac.ecomerce.service.CustomUserDetailService;
import com.tecnotac.ecomerce.service.ProductService;

@Controller
public class HomeController {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CustomUserDetailService customUserDetailService;
	
	public static int productsxpage = 30;
	
	@GetMapping({"home",""})
	public String home(Model model) {
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("productsMostViewed", productService.findAllProductsMostViewed());
		model.addAttribute("productsMostRecent", productService.findAllProductsMostRecent());
		model.addAttribute("productsMostSelled", productService.findAllProductsMostSelled());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/home/clientHome";
	}
	
	@GetMapping("/contact")
	public String contact(Model model) {
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/contact/contact";
	}
	
	@RequestMapping(value = "/api/productspage", method = RequestMethod.GET)
	public ResponseEntity<Page<List<Product>>> getProductsPage(Pageable pageable) {
		return new ResponseEntity<Page<List<Product>>>(productService.findAllProductsPage(pageable), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/productsparent/{id}", method = RequestMethod.GET)
	public ResponseEntity<Page<List<Product>>> getAllByCategoryParent(@PathVariable("id") int id, Pageable pageable) {
		return new ResponseEntity<Page<List<Product>>>(productService.findAllByCategoryParent(id,pageable), HttpStatus.OK);
	}
	
	@GetMapping("/shop")
	public String shop(@RequestParam Map<String, Object> params, Model model) {
		
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		PageRequest pageRequest = PageRequest.of(page, productsxpage);
		Page<List<Product>> pageProduct = productService.findAllProductsPage(pageRequest);
		
		int totalPage = pageProduct.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageProduct.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("countProductByCategoryParent", productService.countProductByCategoryParent());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/shop/shop";
	}
	
	@GetMapping("/search")
	public String search(@RequestParam Map<String, Object> params, Model model, @RequestParam("filter") String filter) {
		
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		PageRequest pageRequest = PageRequest.of(page, productsxpage);
		Page<List<Product>> pageProduct = productService.searchProductsFilterPage(filter,pageRequest);
		
		int totalPage = pageProduct.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageProduct.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("countProductByCategoryParent", productService.countProductByCategoryParent());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/shop/shop";
	}
	
	@RequestMapping(value = "/api/totalproductbycategory/{id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> getTotalProductByCategory(@PathVariable("id") int id) {
		int count = productService.countProductByCategory(id);
		return new ResponseEntity<Integer>(count, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/api/parent/{id}", method = RequestMethod.GET)
//	public ResponseEntity<List<Product>> getAllCategories1(@PathVariable("id") int id) {
//		return new ResponseEntity<List<Product>>(productService.getAllByCategoryParent(id), HttpStatus.OK);
//	}
	
	@RequestMapping(value = "/api/count", method = RequestMethod.GET)
	public ResponseEntity<List<JSONObject>> getAllCategories2() {
		return new ResponseEntity<List<JSONObject>>(productService.countProductByCategoryParent(), HttpStatus.OK);
	}
	
	@GetMapping("/shop/category/{id}")
	public String shopByCategory(@RequestParam Map<String, Object> params, Model model, @PathVariable int id) {
		
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		PageRequest pageRequest = PageRequest.of(page, productsxpage);
		Page<List<Product>> pageProduct = productService.findAllByCategory_Id(id,pageRequest);
		
		int totalPage = pageProduct.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageProduct.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/shop/shop";
	}
	
	@GetMapping("/shop/subcategory1/{id}")
	public String shopBySubCategory1(@RequestParam Map<String, Object> params, Model model, @PathVariable int id) {
		
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		PageRequest pageRequest = PageRequest.of(page, productsxpage);
		Page<List<Product>> pageProduct = productService.findAllBySubCategory1(id,pageRequest);
		
		int totalPage = pageProduct.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageProduct.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/shop/shop";
	}
	
	@GetMapping("/shop/categoryParent/{id}")
	public String shopByCategoryParent(@RequestParam Map<String, Object> params, Model model, @PathVariable int id) {
		
		int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
		PageRequest pageRequest = PageRequest.of(page, productsxpage);
		Page<List<Product>> pageProduct = productService.findAllByCategoryParent(id,pageRequest);
		
		int totalPage = pageProduct.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pageProduct.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("countProductByCategoryParent", productService.countProductByCategoryParent());
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		return "/views/shop/shop";
	}
	
	@GetMapping("/shop/{parent}/viewproduct/{id}")
	public String viewProduct(Model model, @PathVariable int parent, @PathVariable long id) {
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("subcategories", categoryService.getAllSubcategory());
		model.addAttribute("product", productService.getProductById(id).get());
		model.addAttribute("productsMostViewed", productService.findAllProductsMostViewed());
		model.addAttribute("productsRelated", productService.findAllProductsRelated(parent));
		model.addAttribute("cartCount",GlobalData.cartItem.size());
		model.addAttribute("total", GlobalData.cartItem.stream().map(ItemDTO::getSubtotal).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2));
		model.addAttribute("cart",GlobalData.cartItem);
		
		Product product = new Product();
		product.setId(id);
		product.setView(productService.getProductById(id).get().getView()+1);
		productService.updateViewProduct(product);
		
		return "/views/product/viewProduct";
	}
	
}
