package com.tecnotac.ecomerce.myenum;

public enum Status {
	PENDING, COMPLETED, CANCELED, DELIVERED
}
