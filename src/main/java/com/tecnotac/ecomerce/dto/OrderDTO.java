package com.tecnotac.ecomerce.dto;

import java.util.Date;

import lombok.Data;

@Data
public class OrderDTO {

	private int id;
	private String paymethod;
	private String status;
	private Double total;
	private int user_id;
	private int address_id;
	private String number;
	private Date created_at;
	
	public OrderDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymethod() {
		return paymethod;
	}

	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getAddress_id() {
		return address_id;
	}

	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "OrderDTO [id=" + id + ", paymethod=" + paymethod + ", status=" + status + ", total=" + total
				+ ", user_id=" + user_id + ", address_id=" + address_id + ", number=" + number + ", created_at="
				+ created_at + "]";
	}
	
}
