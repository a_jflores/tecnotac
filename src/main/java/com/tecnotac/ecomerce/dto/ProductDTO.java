package com.tecnotac.ecomerce.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import lombok.Data;

@Data
public class ProductDTO {

	private Long id;
	private String name;
	private int categoryId;
	private @Digits(integer = 10, fraction = 2) BigDecimal price;
	private double weight;
	private int stock;
	private String description;
	private String imageName;

	public ProductDTO() {
		super();
	}

	public ProductDTO(Long id, String name, int categoryId, @Digits(integer = 10, fraction = 2) BigDecimal price,
			double weight, int stock, String description, String imageName) {
		super();
		this.id = id;
		this.name = name;
		this.categoryId = categoryId;
		this.price = price;
		this.weight = weight;
		this.stock = stock;
		this.description = description;
		this.imageName = imageName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public @Digits(integer = 10, fraction = 2) BigDecimal getPrice() {
		return price;
	}

	public void setPrice(@Digits(integer = 10, fraction = 2) BigDecimal price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}
	

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	@Override
	public String toString() {
		return "ProductDTO [id=" + id + ", name=" + name + ", categoryId=" + categoryId + ", price=" + price
				+ ", weight=" + weight + ", stock=" + stock + ", description=" + description + ", imageName="
				+ imageName + "]";
	}

}
