package com.tecnotac.ecomerce.dto;

import lombok.Data;

@Data
public class CountProductByCategoryParent {
	private int parent_id;
	private int count;

	public CountProductByCategoryParent() {
		super();
	}
	
	public CountProductByCategoryParent(int parent_id, int count) {
		super();
		this.parent_id = parent_id;
		this.count = count;
	}
	
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "CountProductByCategoryParent [parent_id=" + parent_id + ", count=" + count + "]";
	}
	
}
