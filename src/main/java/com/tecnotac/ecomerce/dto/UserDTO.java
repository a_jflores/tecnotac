package com.tecnotac.ecomerce.dto;

import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.tecnotac.ecomerce.model.Role;

public class UserDTO {
	
	private int id;
	
	@NotBlank
	@Size(min = 3, message = "Mínimo 3 letras")
	private String firstName;
	
	@NotBlank
	@Size(min = 3, message = "Mínimo 3 letras")
	private String lastName;
	
	@NotBlank(message = "Email inválido")
	@Email
	private String email;
	
	@NotBlank(message = "Escriba su contraseña")
	@Size(min = 6, message = "Mínimo 6 caracteres")
	private String password;
	
	@NotBlank(message = "Repita contraseña")
	@Size(min = 6, message = "Mínimo 6 caracteres")
	private String confirmPassword;
	
	private String picture;
	private boolean enabled;
	private List<Role> roles;
	
    public boolean hasRole(String roleName) {
        Iterator<Role> iterator = this.roles.iterator();
        while (iterator.hasNext()) {
            Role role = iterator.next();
            if (role.getName().equals(roleName)) {
                return true;
            }
        }
         
        return false;
    }
	
	public UserDTO() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "UserDTO [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password="
				+ password + ", confirmPassword=" + confirmPassword + ", picture=" + picture + ", enabled=" + enabled
				+ ", roles=" + roles + "]";
	}

}
