package com.tecnotac.ecomerce.dto;

import java.math.BigDecimal;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;

import com.tecnotac.ecomerce.model.Category;

import lombok.Data;

@Data
public class ItemDTO {

	private Long id;
	private String name;
	private @Digits(integer = 10, fraction = 2) BigDecimal price;
	private double weight;
	private int stock;
	private int quantity;
	private String nameImage;
	private int parentCategory;
	
	@Transient
	@Digits(integer = 10, fraction = 2)
	private BigDecimal subtotal;
	private String description;

	public ItemDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNameImage() {
		return nameImage;
	}

	public void setNameImage(String nameImage) {
		this.nameImage = nameImage;
	}

	public int getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(int parentCategory) {
		this.parentCategory = parentCategory;
	}

	@Override
	public String toString() {
		return "ItemDTO [id=" + id + ", name=" + name + ", price=" + price + ", weight=" + weight + ", stock=" + stock
				+ ", quantity=" + quantity + ", nameImage=" + nameImage + ", parentCategory=" + parentCategory
				+ ", subtotal=" + subtotal + ", description=" + description + "]";
	}
	
}
