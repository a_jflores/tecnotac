package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecnotac.ecomerce.model.CustomUserDetail;
import com.tecnotac.ecomerce.model.User;
import com.tecnotac.ecomerce.repository.UserRepository;

@Service
@Transactional
public class CustomUserDetailService implements UserDetailsService{
	
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findUserByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("User Exception"));
		return user.map(CustomUserDetail::new).get();
	}
	
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
	
	public Optional<User> getUserById(Integer id) {
		return userRepository.findById(id);
	}
	
	public Optional<User> getUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}
	
	public Page<List<User>> getClients(Pageable pageable){
		return userRepository.getClients(pageable);
	}
	
	public int totalUsers(){
		return userRepository.totalUsers();
	}
	
	public int totalClients(){
		return userRepository.totalClients();
	}
}
