package com.tecnotac.ecomerce.service;


import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.Order;
import com.tecnotac.ecomerce.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	OrderRepository orderRepository;
	
	public List<Order> getAllOrder(){
		return orderRepository.findAll();
	}
	
	public List<Order> getAllOrderByUser(int id){
		return orderRepository.getAllOrderByUser(id);
	}
	
	public void addOrder(Order order) {
		orderRepository.save(order);
	}
	
	public void removeOrderById(int id) {
		orderRepository.deleteById(id);
	}
	
	public Optional<Order> getOrderById(int id) {
		return orderRepository.findById(id);
	}
	
	public void updateOrder(String status, int id){
		orderRepository.updateOrder(status,id);
	}
	
	public List<JSONObject> getAllOrders(){
		return orderRepository.getAllOrders();
	}
	
	public Page<List<JSONObject>> findPage(Pageable pageable){
        return orderRepository.findPage(pageable);
    }
	
	public BigDecimal totalSales() {
		return orderRepository.totalSales();
	}
	
	public int totalOrders() {
		return orderRepository.totalOrders();
	}
	
	public int completedOrders() {
		return orderRepository.completedOrders();
	}
	
	public int pendingOrders() {
		return orderRepository.pendingOrders();
	}
	
	public int canceledOrders() {
		return orderRepository.canceledOrders();
	}
	
	public int deliveredOrders() {
		return orderRepository.deliveredOrders();
	}

}
