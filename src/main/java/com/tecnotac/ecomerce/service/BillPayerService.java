package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnotac.ecomerce.model.BillPayer;
import com.tecnotac.ecomerce.repository.BillPayerRepository;

@Service
public class BillPayerService {
	
	@Autowired
	BillPayerRepository billPayerRepository;
	
	public List<BillPayer> getAllBillPayer(){
		return billPayerRepository.findAll();
	}
	
	public void addBillPayer(BillPayer billPayer) {
		billPayerRepository.save(billPayer);
	}
	
	public void removeBillPayerById(int id) {
		billPayerRepository.deleteById(id);
	}
	
	public Optional<BillPayer> getBillPayerById(int id) {
		return billPayerRepository.findById(id);
	}
}
