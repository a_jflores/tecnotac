package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tecnotac.ecomerce.model.Image;
import com.tecnotac.ecomerce.repository.ImageRepository;

@Service
public class ImageService {
	
	@Autowired
	ImageRepository imageRepository;
	
	public List<Image> getAllImage(){
		return imageRepository.findAll();
	}
	
	public void addImage(Image image) {
		imageRepository.save(image);
	}
	
	public void removeImageById(int id) {
		imageRepository.deleteById(id);
	}
	
	public Optional<Image> getImageById(int id) {
		return imageRepository.findById(id);
	}
	
}
