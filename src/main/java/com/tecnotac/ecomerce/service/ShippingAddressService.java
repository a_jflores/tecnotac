package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecnotac.ecomerce.model.ShippingAddress;
import com.tecnotac.ecomerce.repository.ShippingAddressRepository;

@Service
public class ShippingAddressService {
	
	@Autowired
	ShippingAddressRepository shippingAddressRepository;
	
	public List<ShippingAddress> getAllShippingAddress(){
		return shippingAddressRepository.findAll();
	}
	
	public void addShippingAddress(ShippingAddress shippingAddress) {
		shippingAddressRepository.save(shippingAddress);
	}
	
	public void removeShippingAddressById(int id) {
		shippingAddressRepository.deleteById(id);
	}
	
	public Optional<ShippingAddress> getShippingAddressById(int id) {
		return shippingAddressRepository.findById(id);
	}
}
