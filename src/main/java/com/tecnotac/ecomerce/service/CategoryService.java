package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tecnotac.ecomerce.model.Category;
import com.tecnotac.ecomerce.repository.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	public List<Category> getAllCategory(){
		return categoryRepository.findAll();
	}
	
	public void addCategory(Category category) {
		categoryRepository.save(category);
	}
	
	public void removeCategoryById(int id) {
		categoryRepository.deleteById(id);
	}
	
	public Optional<Category> getCategoryById(int id) {
		return categoryRepository.findById(id);
	}
	
	public List<Category> getAllCategoryParent() {
		return categoryRepository.getAllCategoryParent();
	}
	
	public List<Category> getAllCategoryChildren(int id) {
		return categoryRepository.getAllCategoryChildren(id);
	}

	public List<Category> getAllSubcategory() {
		return categoryRepository.getAllSubcategory();
	}
	
}
