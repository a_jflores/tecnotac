package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.Product;
import com.tecnotac.ecomerce.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	public List<Product> getAllProduct(){
		return productRepository.findAll();
	}
	
	public void addProduct(Product product) {
		productRepository.save(product);
	}
	
	public void removeProductById(long id) {
		productRepository.deleteById(id);
	}
	
	public Optional<Product> getProductById(long id) {
		return productRepository.findById(id);
	}
	
	public Page<List<Product>> findAllByCategory_Id(int id, Pageable pageable){
		return productRepository.findAllByCategory_Id(id, pageable);
	}
	
	public Page<List<Product>> findAllByCategoryParent(int id, Pageable pageable){
		return productRepository.findAllByCategoryParent(id, pageable);
	}
	
	public Page<List<Product>> findAllBySubCategory1(int id, Pageable pageable){
		return productRepository.findAllBySubCategory1(id, pageable);
	}
	
	public int countProductByCategory(int id){
		return productRepository.countProductByCategory(id);
	}
	
	public List<JSONObject> countProductByCategoryParent(){
		return productRepository.countProductByCategoryParent();
	}
	
	public void updateStockProduct(Product product){
		productRepository.updateStockProduct(product.getStock(),product.getId());
	}
	
	public List<String> searchProducts(){
		return productRepository.searchProducts();
	}
	
	public Page<List<Product>> searchProductsFilterPage(String filter, Pageable pageable){
		return productRepository.searchProductsFilterPage(filter, pageable);
	}
	
	public void updateViewProduct(Product product){
		productRepository.updateViewProduct(product.getView(),product.getId());
	}
	
	public List<Product> findAllProductsMostViewed(){
		return productRepository.findAllProductsMostViewed();
	}
	
	public List<Product> findAllProductsMostRecent(){
		return productRepository.findAllProductsMostRecent();
	}
	
	public List<Product> findAllProductsMostSelled(){
		return productRepository.findAllProductsMostSelled();
	}
	
	public List<Product> findAllProductsRelated(int parent){
		return productRepository.findAllProductsRelated(parent);
	}
	
	public Page<List<Product>> findAllProductsPage(Pageable pageable){
		return productRepository.findAllProductsPage(pageable);
	}
	
}
