package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tecnotac.ecomerce.model.Transaction;
import com.tecnotac.ecomerce.repository.TransactionRepository;

@Service
public class TransactionService {
	
	@Autowired
	TransactionRepository transactionRepository;
	
	public List<Transaction> getAllTransaction(){
		return transactionRepository.findAll();
	}
	
	public void addTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}
	
	public void removeTransactionById(int id) {
		transactionRepository.deleteById(id);
	}
	
	public Optional<Transaction> getTransactionById(int id) {
		return transactionRepository.findById(id);
	}
}
