package com.tecnotac.ecomerce.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.OrderItems;
import com.tecnotac.ecomerce.repository.OrderItemsRepository;

@Service
public class OrderItemsService {
	
	@Autowired
	OrderItemsRepository orderItemsRepository;
	
	public List<OrderItems> getAllOrderItems(){
		return orderItemsRepository.findAll();
	}
	
	public void addOrderItems(OrderItems orderitems) {
		orderItemsRepository.save(orderitems);
	}
	
	public void removeOrderItemsById(int id) {
		orderItemsRepository.deleteById(id);
	}
	
	public Optional<OrderItems> getOrderItemsById(int id) {
		return orderItemsRepository.findById(id);
	}
	
	public List<JSONObject> bestSellingProduct(){
		return orderItemsRepository.bestSellingProduct();
	}
}
