package com.tecnotac.ecomerce.exception;

public class CustomEmailValidationException extends Exception{
	
	private static final long serialVersionUID = -8114146932074002725L;
	private String email;
	
	public CustomEmailValidationException(String message, String email) {
		super(message);
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
}
