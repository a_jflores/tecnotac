package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecnotac.ecomerce.model.ShippingAddress;

public interface ShippingAddressRepository extends JpaRepository<ShippingAddress, Integer>{

}
