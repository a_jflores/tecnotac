package com.tecnotac.ecomerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.OrderItems;

public interface OrderItemsRepository extends JpaRepository<OrderItems, Integer>{
	
	@Query(value = "SELECT\r\n"
			+ "	P.id,\r\n"
			+ "	P.name,\r\n"
			+ "	P.stock,\r\n"
			+ "	SUM(quantity) quantitySales\r\n"
			+ "FROM\r\n"
			+ "	product P\r\n"
			+ "INNER JOIN order_items OI ON OI.product_id = P.id\r\n"
			+ "GROUP BY\r\n"
			+ "	P.id\r\n"
			+ "ORDER BY\r\n"
			+ "	quantitySales DESC\r\n"
			+ "LIMIT 8", nativeQuery = true)
	List<JSONObject> bestSellingProduct();

}