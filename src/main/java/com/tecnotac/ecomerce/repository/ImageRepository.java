package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.tecnotac.ecomerce.model.Image;

public interface ImageRepository extends JpaRepository<Image, Integer>{
	
}