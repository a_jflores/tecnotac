package com.tecnotac.ecomerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tecnotac.ecomerce.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>{
	  @Query(value = "select * from category where parent_id is NULL", nativeQuery = true)
	  List<Category> getAllCategoryParent();
	  
	  @Query(value = "select * from category where parent_id is NOT NULL", nativeQuery = true)
	  List<Category> getAllSubcategory();
	  
	  @Query(value = "select * from category where parent_id = ?", nativeQuery = true)
	  List<Category> getAllCategoryChildren(int id);
	  
}