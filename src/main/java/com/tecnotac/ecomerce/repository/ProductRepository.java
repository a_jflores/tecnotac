package com.tecnotac.ecomerce.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	
	Page<List<Product>> findAllByCategory_Id(int id, Pageable pageable);
	
	@Query(value = "SELECT\r\n"
			+ "	p.*,\r\n"
			+ "	c2.parent_id\r\n"
			+ "FROM\r\n"
			+ "	product p\r\n"
			+ "LEFT JOIN category c1 ON c1.category_id = p.category_id\r\n"
			+ "LEFT JOIN category c2 ON c2.category_id = c1.parent_id\r\n"
			+ "WHERE\r\n"
			+ "	c2.parent_id = :id", nativeQuery = true )
	Page<List<Product>> findAllByCategoryParent(@Param("id") int id, Pageable pageable);
	
	@Query(value = "SELECT * FROM category c INNER JOIN product p ON p.category_id = c.category_id WHERE c.parent_id = ?", nativeQuery = true )
	Page<List<Product>> findAllBySubCategory1(int id, Pageable pageable);
	
	@Query(value = "SELECT\r\n"
			+ "	count(*)\r\n"
			+ "FROM\r\n"
			+ "	product p\r\n"
			+ "LEFT JOIN category c1 ON c1.category_id = p.category_id\r\n"
			+ "LEFT JOIN category c2 ON c2.category_id = c1.parent_id\r\n"
			+ "WHERE\r\n"
			+ "	c2.parent_id = ?", nativeQuery = true )
	int countProductByCategory(int id);
	
	@Query( value = "SELECT\r\n"
			+ "	c2.parent_id as id,\r\n"
			+ "	c3.name,\r\n"
			+ "	COUNT(*) AS count\r\n"
			+ "FROM\r\n"
			+ "	category c1\r\n"
			+ "LEFT JOIN category c2 ON c2.category_id = c1.parent_id\r\n"
			+ "INNER JOIN product p ON p.category_id = c1.category_id\r\n"
			+ "INNER JOIN category c3 ON c3.category_id = c2.parent_id\r\n"
			+ "GROUP BY\r\n"
			+ "	c2.parent_id", nativeQuery = true)
	List<JSONObject> countProductByCategoryParent();
	
	@Modifying
	@Transactional
	@Query( value = "UPDATE product SET stock = ? WHERE id = ?", nativeQuery = true)
	void updateStockProduct(@Param("stock") Integer stock, @Param("id") Long id);
	
	@Query( value = "SELECT name FROM product", nativeQuery = true)
	List<String> searchProducts();
	
	@Query( value = "SELECT * FROM product WHERE name LIKE %:filter%", nativeQuery = true, countQuery = "SELECT COUNT(*) FROM product")
	Page<List<Product>> searchProductsFilterPage(@Param("filter") String filter, Pageable pageable);
	
	@Modifying
	@Transactional
	@Query( value = "UPDATE product SET view = ? WHERE id = ?", nativeQuery = true)
	void updateViewProduct(@Param("view") Integer view, @Param("id") Long id);
	
	@Query(value = "SELECT * FROM product ORDER BY VIEW DESC LIMIT 8", nativeQuery = true )
	List<Product> findAllProductsMostViewed();
	
	@Query(value = "SELECT * FROM product ORDER BY created_at DESC LIMIT 8", nativeQuery = true )
	List<Product> findAllProductsMostRecent();
	
	@Query(value = "SELECT\r\n"
			+ "P.*,\r\n"
			+ "SUM(quantity) quantitySales\r\n"
			+ "FROM\r\n"
			+ "product P\r\n"
			+ "INNER JOIN order_items OI ON OI.product_id = P.id\r\n"
			+ "GROUP BY\r\n"
			+ "P.id\r\n"
			+ "ORDER BY\r\n"
			+ "quantitySales DESC\r\n"
			+ "LIMIT 8", nativeQuery = true )
	List<Product> findAllProductsMostSelled();
	
	@Query(value = "SELECT\r\n"
			+ "	p.*\r\n"
			+ "FROM\r\n"
			+ "	category c1\r\n"
			+ "LEFT JOIN category c2 ON c2.category_id = c1.parent_id\r\n"
			+ "INNER JOIN product p ON p.category_id = c1.category_id\r\n"
			+ "INNER JOIN category c3 ON c3.category_id = c2.parent_id\r\n"
			+ "WHERE c2.parent_id = ? LIMIT 5", nativeQuery = true )
	List<Product> findAllProductsRelated(int parent);
	
	@Query(value = "SELECT * FROM product", nativeQuery = true )
	Page<List<Product>> findAllProductsPage(Pageable pageable);

}