package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.tecnotac.ecomerce.model.BillPayer;

public interface BillPayerRepository extends JpaRepository<BillPayer, Integer>{

}