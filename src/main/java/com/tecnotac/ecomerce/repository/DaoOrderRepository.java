package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import com.tecnotac.ecomerce.model.Order;

public interface DaoOrderRepository extends DataTablesRepository<Order, Integer>{

}
