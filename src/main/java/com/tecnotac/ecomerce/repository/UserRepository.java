package com.tecnotac.ecomerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.tecnotac.ecomerce.model.User;

@Transactional
public interface UserRepository extends JpaRepository<User, Integer>{
	Optional<User> findUserByEmail(String email);
	
	@Query(value = "SELECT * FROM users u INNER JOIN user_role ur ON ur.user_id = u.id WHERE ur.role_id = 3", nativeQuery = true )
	Page<List<User>> getClients(Pageable pageable);
	
	@Query(value = "SELECT * FROM users u INNER JOIN user_role ur ON ur.user_id = u.id WHERE ur.role_id < 3", nativeQuery = true )
	int totalUsers();
	
	@Query(value = "SELECT * FROM users u INNER JOIN user_role ur ON ur.user_id = u.id WHERE ur.role_id = 3", nativeQuery = true )
	int totalClients();
	
}
