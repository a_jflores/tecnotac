package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import com.tecnotac.ecomerce.model.User;

public interface DaoUserRepository extends DataTablesRepository<User, Integer>{

}
