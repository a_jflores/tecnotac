package com.tecnotac.ecomerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.tecnotac.ecomerce.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

}