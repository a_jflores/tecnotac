package com.tecnotac.ecomerce.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.tecnotac.ecomerce.model.Order;


@Transactional
public interface OrderRepository extends JpaRepository<Order, Integer>{
	
	@Query(value = "SELECT * FROM orders WHERE user_id = ?", nativeQuery = true)
	List<Order> getAllOrderByUser(int id);
	
	@Query(value = "SELECT * FROM orders", countQuery = "SELECT COUNT(*) FROM orders", nativeQuery = true)
	Page<List<JSONObject>> findPage(Pageable pageable);
	
	@Query(value = "SELECT * FROM orders", nativeQuery = true)
	List<JSONObject> getAllOrders();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE orders SET status = ? WHERE id = ?", nativeQuery = true)
	void updateOrder(@Param("status") String status, @Param("id") int id);
	
	@Query(value = "SELECT SUM(total) FROM orders WHERE STATUS = 'COMPLETED'", nativeQuery = true)
	BigDecimal totalSales();
	
	@Query(value = "SELECT COUNT(*) FROM orders", nativeQuery = true)
	int totalOrders();
	
	@Query(value = "SELECT COUNT(*) FROM orders WHERE status = 'COMPLETED'", nativeQuery = true)
	int completedOrders();
	
	@Query(value = "SELECT COUNT(*) FROM orders WHERE status = 'PENDING'", nativeQuery = true)
	int pendingOrders();
	
	@Query(value = "SELECT COUNT(*) FROM orders WHERE status = 'CANCELED'", nativeQuery = true)
	int canceledOrders();
	
	@Query(value = "SELECT COUNT(*) FROM orders WHERE status = 'DELIVERED'", nativeQuery = true)
	int deliveredOrders();
	
}