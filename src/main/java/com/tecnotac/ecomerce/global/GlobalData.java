package com.tecnotac.ecomerce.global;

import java.util.ArrayList;
import java.util.List;

import com.tecnotac.ecomerce.dto.ItemDTO;
import com.tecnotac.ecomerce.model.User;

public class GlobalData {
	
//	public static List<Product> cart;
	public static List<User> user;
	public static List<String> izipay;
	public static List<ItemDTO> cartItem;
	
//	static {
//		cart = new ArrayList<Product>();
//	}
	
	static {
		user = new ArrayList<User>();
	}
	
	static {
		izipay = new ArrayList<String>();
	}
	
	static {
		cartItem = new ArrayList<ItemDTO>();
	}
}
