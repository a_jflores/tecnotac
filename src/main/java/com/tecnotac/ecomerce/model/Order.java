package com.tecnotac.ecomerce.model;
import com.tecnotac.ecomerce.myenum.Status;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tecnotac.ecomerce.myenum.PaymentMethod;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String number;
	
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name="user_id")
    @JsonIgnoreProperties("user_id")
	private User user;
    
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name="billpayer_id")
    @JsonIgnoreProperties("billpayer_id")
	private BillPayer billpayer;
    
    @Enumerated(EnumType.STRING)
	private PaymentMethod paymentMethod;
    
	private BigDecimal total;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany(mappedBy = "order")
	@JsonIgnoreProperties("order")
	private List<OrderItems> orderItems;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="transaction_id")
	@JsonIgnoreProperties("transaction_id")
	private Transaction transaction;
	
	private String note;
	private Date created_at;

	public Order() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public BillPayer getBillpayer() {
		return billpayer;
	}

	public void setBillpayer(BillPayer billpayer) {
		this.billpayer = billpayer;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<OrderItems> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItems> orderItems) {
		this.orderItems = orderItems;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", number=" + number + ", user=" + user + ", billpayer=" + billpayer
				+ ", paymentMethod=" + paymentMethod + ", total=" + total + ", status=" + status + ", orderItems="
				+ orderItems + ", transaction=" + transaction + ", note=" + note + ", created_at=" + created_at + "]";
	}
	
}
