package com.tecnotac.ecomerce.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Entity
@Data
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	
	@ManyToOne
	@JsonIgnoreProperties("category_id")
	@JoinColumn(name = "category_id", referencedColumnName = "category_id")
	private Category category;

	@Column(precision = 10, scale = 2)
	@Digits(integer = 10, fraction = 2)
	private BigDecimal price;

	private double weight;
	
	@Column(columnDefinition="TEXT")
	private String description;
	private int stock;
	private int view;

	@Transient
	private int quantity;

	@Transient
	@Digits(integer = 10, fraction = 2)
	private BigDecimal subtotal;
	
	@JsonIgnoreProperties("product")
	@OneToMany(mappedBy = "product")
	private List<OrderItems> orderItems;
	
	@JsonIgnoreProperties("product")
	@OneToMany(mappedBy = "product")
	private List<Image> image;
	
	private Date created_at;

	public Product() {
		super();
	}

	public Product(Long id, String name, Category category, @Digits(integer = 10, fraction = 2) BigDecimal price,
			double weight, String description,List<Image> image) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price;
		this.weight = weight;
		this.description = description;
		this.image = image;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public @Digits(integer = 10, fraction = 2) BigDecimal getPrice() {
		return price;
	}

	public void setPrice(@Digits(integer = 10, fraction = 2) BigDecimal price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public int getView() {
		return view;
	}

	public void setView(int view) {
		this.view = view;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public List<Image> getImage() {
		return image;
	}

	public void setImage(List<Image> image) {
		this.image = image;
	}
	
	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", category=" + category + ", price=" + price + ", weight="
				+ weight + ", description=" + description + ", stock=" + stock + ", view=" + view + ", quantity="
				+ quantity + ", subtotal=" + subtotal + ", orderItems=" + orderItems + ", image=" + image
				+ ", created_at=" + created_at + "]";
	}

}
