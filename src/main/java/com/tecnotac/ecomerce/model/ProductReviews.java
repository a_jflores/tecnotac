package com.tecnotac.ecomerce.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "product_reviews")
public class ProductReviews {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "product_id")
	private Product product;
	
	private int rating;
	private String description;
	private Date created_at;
	
	public ProductReviews() {
		super();
	}

	public ProductReviews(Integer id, User user, Product product, int rating, String description, Date created_at) {
		super();
		this.id = id;
		this.user = user;
		this.product = product;
		this.rating = rating;
		this.description = description;
		this.created_at = created_at;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "ProductReviews [id=" + id + ", user=" + user + ", product=" + product + ", rating=" + rating
				+ ", description=" + description + ", created_at=" + created_at + "]";
	}
	
}
